defmodule BarrelEx do
  @moduledoc """
  Documentation for BarrelEx.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BarrelEx.hello
      :world

  """
  def hello do
    :world
  end
end
